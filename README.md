## Contact us rctiplus kotlin

#### Cara Pembuatan UI Contact Us rctiplus

##### 1. Add Library TextInputEditText & TextInputLayout, pada app -> build.gradle
```
// TextInputLayout & TextInputEditText library
    implementation 'com.google.android.material:material:1.2.1'
```

##### 2. Membuat UI pada MainActivity.xml
 ```
<?xml version="1.0" encoding="utf-8"?>
<ScrollView xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:background="@color/colorDarkGrey"
    tools:context=".MainActivity">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="vertical"
        android:padding="20dp">

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="20dp"
            android:fontFamily="sans-serif-light"
            android:lineSpacingMultiplier="1.5"
            android:paddingHorizontal="20dp"
            android:text="@string/title_contact_us"
            android:textColor="@color/colorWhite"
            android:textSize="20sp" />

        <com.google.android.material.textfield.TextInputLayout
            android:layout_width="match_parent"
            android:layout_height="45dp"
            android:layout_marginHorizontal="20dp"
            android:layout_marginTop="20dp"
            android:gravity="center"
            android:textColorHint="@color/colorWhiteGray"
            app:hintAnimationEnabled="true"
            app:hintTextAppearance="@style/HintText">

            <com.google.android.material.textfield.TextInputEditText
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:backgroundTint="@color/colorWhiteGray2"
                android:hint="@string/form_username"
                android:textColor="@color/colorWhite"
                android:textSize="13sp" />

        </com.google.android.material.textfield.TextInputLayout>

        <com.google.android.material.textfield.TextInputLayout
            android:layout_width="match_parent"
            android:layout_height="45dp"
            android:layout_marginHorizontal="20dp"
            android:gravity="center"
            android:textColorHint="@color/colorWhiteGray"
            app:hintAnimationEnabled="true"
            app:hintTextAppearance="@style/HintText">

            <com.google.android.material.textfield.TextInputEditText
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:backgroundTint="@color/colorWhiteGray2"
                android:hint="@string/form_email"
                android:textColor="@color/colorWhite"
                android:textSize="13sp" />

        </com.google.android.material.textfield.TextInputLayout>

        <com.google.android.material.textfield.TextInputLayout
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:layout_marginHorizontal="20dp"
            android:textColorHint="@color/colorWhiteGray"
            app:hintAnimationEnabled="true"
            app:hintTextAppearance="@style/HintText">

            <com.google.android.material.textfield.TextInputEditText
                android:layout_width="match_parent"
                android:layout_height="150dp"
                android:backgroundTint="@color/colorWhiteGray2"
                android:hint="@string/form_stories"
                android:textColor="@color/colorWhite"
                android:textSize="13sp" />

        </com.google.android.material.textfield.TextInputLayout>

        <Button
            android:layout_width="match_parent"
            android:layout_height="35dp"
            android:layout_gravity="center"
            android:layout_marginHorizontal="20dp"
            android:layout_marginTop="30dp"
            android:background="@drawable/layout_bg"
            android:text="@string/send"
            android:textAllCaps="false"
            android:textColor="@color/colorWhite" />

    </LinearLayout>

</ScrollView>
```

##### 3. Membuat layout_bg pada drawable untuk background pada Button "Send"
```
<?xml version="1.0" encoding="UTF-8"?>
<shape xmlns:android="http://schemas.android.com/apk/res/android">
    <solid android:color="#FF2222" />
    <stroke
        android:width="3dp"
        android:color="#FF2222" />
    <corners android:radius="5dp" />
    <padding
        android:bottom="0dp"
        android:left="0dp"
        android:right="0dp"
        android:top="0dp" />
</shape>
```

##### 4. Membuat colors.xml pada value
```
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <color name="colorPrimary">#3A3A3A</color>
    <color name="colorPrimaryDark">#3700B3</color>
    <color name="colorAccent">#03DAC5</color>
    <color name="colorWhiteGray">#777777</color>
    <color name="colorWhiteGray2">#9C9C9C</color>
    <color name="colorGray">#3A3A3A</color>
    <color name="colorDarkGrey">#252525</color>
    <color name="colorWhite">#FFFF</color>
</resources>
```

##### 5. Membuat strings.xml pada value
```
<resources>
    <string name="app_name">Contact Us</string>
    <string name="form_username">Full Name</string>
    <string name="form_phoneNumber">Phone Number</string>
    <string name="form_stories">Your Stories</string>
    <string name="form_email">Email</string>
    <string name="title_contact_us">We\'re happy to listen your stories and help your problem.</string>
    <string name="send">Send</string>
    <string name="floating_hint_disabled">Floating Hint Disabled</string>
</resources>
```

##### 6. Membuat styles.xml pada value
```
<style name="HintText" parent="TextAppearance.Design.Hint">
    <item name="android:textSize">11sp</item>
    <item name="android:textColor">@color/colorWhite</item>
</style>
```







